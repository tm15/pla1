package ga.manuelgarciacr.consultoridefrases;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import ga.manuelgarciacr.consultoridefrases.model.Constants;
import ga.manuelgarciacr.consultoridefrases.model.Frase;
import ga.manuelgarciacr.consultoridefrases.model.FraseDao;

public class FraseDetailActivity extends AppCompatActivity {
    private FraseDao fraseDao = new FraseDao();

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frase_detail);

        TextView txvId = findViewById(R.id.txv_id);
        TextView txvAuthor = findViewById(R.id.txv_author);
        TextView txvText = findViewById(R.id.txv_text);
        Frase frase = fraseDao.getFraseById(getIntent().getIntExtra(Constants.EXTRA_INTENT_FRASE_DETAIL, 1));
        txvText.setText(frase.getText());
        txvAuthor.setText(frase.getAuthor());
        txvId.setText(String.format("%d", frase.getId()));

    }
}
