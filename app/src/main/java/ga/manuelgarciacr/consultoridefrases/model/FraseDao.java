package ga.manuelgarciacr.consultoridefrases.model;

import android.annotation.TargetApi;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FraseDao {
    private ArrayList<Frase> mFrases;

    public FraseDao() {
        int id = 0;

        mFrases = new ArrayList<>();
        mFrases.add(new Frase(++id, "Anònim", "Anar a la seva", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Anònim", "Anar a l’encalç", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Anònim", "Anar a pams", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Anònim", "Anar a pas de bou", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Anònim", "Anar a pastar fang", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Anònim", "Anar aigua avall", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Anònim", "Anar brut com una guilla", Constants.FRASES_FETES));
        mFrases.add(new Frase(++id, "Miquel Martí i Pol", "Tot està per fer i tot és possible", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "Ferran Salmurri", "La funció del pare és ensenyar els fills a ser més feliços", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "Pulitzer", "Compte amb les situacions inesperades. En elles s’amaguen la nostra gran oportunitat", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "Proverbi japonès", "El que parla sembra, el que escolta recull.", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "Anònim", "Quan una cosa dolenta et succeeixi tens dues opcions: Deixar que et destrueixi o deixar que et faci més fort.", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "León Lederman", "Tota acció provoca reaccions", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "Ferran Salmurri", "El valor que tenim radica en el que pensem de nosaltres mateixos i no en el que creiem que els altres pensen de nosaltres", Constants.CITES_CELEBRES));
        mFrases.add(new Frase(++id, "Sèneca", "Tria per mestre a aquell a qui admiris més pel que en ell veiessis que pel que escoltessis dels seus llavis.", Constants.CITES_CELEBRES));
    }

    @TargetApi(24) public List<Frase> getFrases(){
        if(Build.VERSION.SDK_INT < 24)
            return setFilter(Constants.FRASES_FETES);
        else
            return mFrases.stream().filter(frase -> frase.isType(Constants.FRASES_FETES)).collect(Collectors.toList());
    }

    public List<Frase> getConsells(){
        if(Build.VERSION.SDK_INT < 24)
            return setFilter(Constants.CITES_CELEBRES);
        else
            return mFrases.stream().filter(frase -> frase.isType(Constants.CITES_CELEBRES)).collect(Collectors.toList());
    }

    private List<Frase> setFilter(int tipus) {
        List<Frase> result = new ArrayList<>();
        for (Frase temp : mFrases) {
            if (temp.getType() == tipus) {
                result.add(temp);
            }
        }
        return result;
    }

    public Frase getFraseById(int id) {
        for (Frase temp : mFrases) {
            if (temp.getId() == id) {
                return temp;
            }
        }
        return mFrases.get(1);
    }
}
