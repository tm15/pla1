package ga.manuelgarciacr.consultoridefrases.model;

public interface Constants {
    int FRASES_FETES = 1;
    int CITES_CELEBRES = 2;
    String PACKAGE_NAME = "ga.manuelgarciacr.consultoridefrases";
    String EXTRA_INTENT_FRASE_DETAIL = PACKAGE_NAME + ".extra.intent.frase.detail";
}
