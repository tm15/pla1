package ga.manuelgarciacr.consultoridefrases;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
//import android.widget.Toast;

import java.util.List;
import java.util.Random;

import ga.manuelgarciacr.consultoridefrases.model.Constants;
import ga.manuelgarciacr.consultoridefrases.model.Frase;
import ga.manuelgarciacr.consultoridefrases.model.FraseDao;

public class MainActivity extends AppCompatActivity {
    private static final String KEY_FRASE = "frase";
    FraseDao fraseDao;
    Frase mFrase;
    int idxOldFrasesFetes = 0;
    int idxOldConsells = 0;
    TextView txv_output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_consells = findViewById(R.id.btn_consells);
        Button btn_frasesFetes = findViewById(R.id.btn_frasesFetes);
        txv_output = findViewById(R.id.txv_output);
        fraseDao = new FraseDao();
        if (savedInstanceState != null) {
            int id = savedInstanceState.getInt(KEY_FRASE, 0);
            if(id > 0) {
                mFrase = fraseDao.getFraseById(id);
                txv_output.setText(mFrase.getText());
            }
        }

        btn_consells.setOnClickListener(v -> {

            List<Frase> lstFrases = fraseDao.getConsells();
            int idx;
            do {
                idx = (new Random()).nextInt(lstFrases.size());
            }while(idx == idxOldConsells);
            idxOldConsells = idx;

            mFrase = lstFrases.get(idx);
            txv_output.setText(mFrase.getText());
            //Toast.makeText(getApplicationContext(), "btn_consells. " + idx,Toast.LENGTH_SHORT).show();
        });

        btn_frasesFetes.setOnClickListener(v -> {
            List<Frase> lstFrases = fraseDao.getFrases();
            int idx;
            do {
                idx = (new Random()).nextInt(lstFrases.size());
            }while(idx == idxOldFrasesFetes);
            idxOldFrasesFetes = idx;

            mFrase =lstFrases.get(idx);
            txv_output.setText(mFrase.getText());
            //Toast.makeText(getApplicationContext(), "btn_frasesFetes. " + idx,Toast.LENGTH_SHORT).show();
        });

        txv_output.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, FraseDetailActivity.class);
            intent.putExtra(Constants.EXTRA_INTENT_FRASE_DETAIL, mFrase.getId());
            startActivity(intent);
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(mFrase != null)
            savedInstanceState.putInt(KEY_FRASE, mFrase.getId());
    }
}
