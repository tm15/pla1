package ga.manuelgarciacr.consultoridefrases.model;

public class Frase {
    private int mId;
    private String mText;
    private String mAuthor;
    private int mType;

    Frase(int id, String author, String text, int type) {
        setId(id);
        setAuthor(author);
        setText(text);
        setType(type);
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getText() {
        return mText;
    }

    private void setText(String mText) {
        this.mText = mText;
    }

    public String getAuthor() {
        return mAuthor;
    }

    private void setAuthor(String mAuthor) {
        this.mAuthor = mAuthor;
    }

    int getType() {
        return mType;
    }

    private void setType(int mType) {
        this.mType = mType;
    }

    boolean isType(int type){
        return getType() == type;
    }
}
